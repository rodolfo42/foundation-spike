$(function(){
	var MyApp = new Backbone.Marionette.Application();

	Backbone.Marionette.TemplateCache.prototype.compileTemplate = function(rawTemplate) {
		return Handlebars.compile(rawTemplate);
	};

	MyApp.addRegions({
		main: '#main',
		form: '#form',
		table: '#content'
	});

	var FormView = Backbone.Marionette.ItemView.extend({
	  tagName: "form",
	  template: "#form-tpl",
	  events: {
	  	'keyup :input': 'atualizarModel',
	  	'submit': 'save'
	  },
	  atualizarModel: function() {
	  	var nome = this.$('[name="nome"]').val();
	  	this.model.set('name', nome);

	  	var sobrenome = this.$('[name="sobrenome"]').val();
	  	this.model.set('sobrenome', sobrenome);
	  },
	  save: function(e) {
	  	e.preventDefault();
	  	MyApp.vent.trigger('save', this.model);
	  }
	});

	var FormData = Backbone.Model.extend({
		nome: function() {
			return this.attributes.name + " " + this.attributes.sobrenome;
		}
	});

	MyApp.addInitializer(function(){
		var ricardo = new FormData({
			name: 'Ricardo',
			sobrenome: 'Lino'
		});

		ricardo.bind('change:name', function(e){
			console.log( 'nome mudou para ' + this.get('name') );
		});

		var form = new FormView({
			model: ricardo
		});

		MyApp.form.show(form);
	});

	MyApp.addInitializer(function(){
		MyApp.vent.bind('save', function(m){
				console.log( m.toJSON() );
			});
	});

	MyApp.start();

	window.app = MyApp;
});